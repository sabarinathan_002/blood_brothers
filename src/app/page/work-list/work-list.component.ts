import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../services/http.service'
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-work-list',
  templateUrl: './work-list.component.html',
  styleUrls: ['./work-list.component.scss']
})
export class WorkListComponent implements OnInit {

  constructor(private http: HttpService, public router: Router, private acRoute: ActivatedRoute) { }
  search: any = '';
  work_list: Array<any> = []
  page_count:any = 10;
  total_item:any = 30;
  current_page:any = 1;

  ngOnInit(): void {
    localStorage.setItem('completed','1')
    this.getworkList()
  }

  //Worklist get
  getworkList() {
    console.log('',this.search)
    var body = {
      search: this.search,
      pagecount:this.page_count,
      pagevalue:this.current_page
    }
    this.http.post('api/admin/dietitian-worklist', body).subscribe((data: any) => {
      if (data.status == 1) {
        this.total_item = data.data_count
        this.work_list = data.data
        this.work_list = this.work_list.reverse()
        console.log(this.work_list)
      }
      if (data.status == 0) {
        this.work_list = []
       }
    })
  }

  searchFunction() {
    if (this.search.length >= 2) {
      this.getworkList()
    }
    else if(this.search == " " || null || undefined){
      this.getworkList()
    }
  }

  //navigate to coresponding test kit id, dietitian report
  clickworkList(value: any, kit_id: any) {
    localStorage.setItem('UserId', value)
    this.searchfn(kit_id)
    console.log(value)
    localStorage.setItem('side-bar-id',value)
    setTimeout(() => {
      localStorage.setItem('completed', '1')
      this.router.navigate(["/page/dietitian/lifestyle"])
    }, 750);
  }

  //Worklist search function
  searchfn(value: any) {
    let body = {
      testkit_id: value
    }
    this.http.post('api/admin/testkit/search', body).subscribe((data: any) => {
      JSON.stringify(localStorage.setItem('testkit_id', JSON.stringify(data)))
      // var data1 = JSON.parse(localStorage.getItem('testkit_id')!)
      console.log(data)
    })
  }

  paginatorChange(value:any){
    console.log(value)
    this.page_count = value.rows
    this.current_page = value.page + 1
    this.getworkList()
  }

  errorFunction(data: any) {
    console.log(data)
    var message = data.message.name
    if (data.message.name === 'TokenExpiredError') {
      localStorage.clear();
      this.router.navigate([`login`]);
    }
  }


}
