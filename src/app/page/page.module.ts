import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbMenuModule } from "@nebular/theme";
import { FormsModule, ReactiveFormsModule  } from "@angular/forms";
import { PageRoutingModule } from './page-routing.module';
import { PageComponent } from './page.component';
import { ThemeModule } from '../theme/theme.module';
import { DietitianTabComponent } from './dietitian-tab/dietitian-tab.component';
import { ViewDraftComponent } from './view-draft/view-draft.component';
import { WorkListComponent } from './work-list/work-list.component';
import { CompletedWorkComponent } from './completed-work/completed-work.component';

import {PaginatorModule} from 'primeng/paginator';



@NgModule({
  declarations: [
    PageComponent,
    ViewDraftComponent,
    WorkListComponent,
    CompletedWorkComponent,
  ],
  imports: [
    CommonModule,
    PageRoutingModule,
    ThemeModule,
    NbMenuModule,
    FormsModule,
    ReactiveFormsModule,
    PaginatorModule
  ]
})
export class PageModule { }
