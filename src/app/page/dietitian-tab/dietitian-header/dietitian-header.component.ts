import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dietitian-header',
  templateUrl: './dietitian-header.component.html',
  styleUrls: ['./dietitian-header.component.scss']
})
export class DietitianHeaderComponent implements OnInit {

  
  constructor(public router: Router) {  }

  subheader_details: any;
  age:any;
  completed_id:any;

  ngOnInit(): void {
    var data1 = localStorage.getItem('testkit_id')!
    // console.log(JSON.parse(data1))
    this.subheader_details = JSON.parse(data1)
    this.completed_id=localStorage.getItem('completed')

    var dob = new Date(this.subheader_details.dob)
    var currentYear = new Date()
    this.age = currentYear.getUTCFullYear() - dob.getUTCFullYear()
    
  }

  logout(){
    localStorage.clear();
    this.router.navigate([`login`]);
  }
  

}
