import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DietitianHeaderComponent } from './dietitian-header.component';

describe('DietitianHeaderComponent', () => {
  let component: DietitianHeaderComponent;
  let fixture: ComponentFixture<DietitianHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DietitianHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DietitianHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
