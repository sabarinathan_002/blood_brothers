import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SleepTabComponent } from './sleep-tab.component';

describe('SleepTabComponent', () => {
  let component: SleepTabComponent;
  let fixture: ComponentFixture<SleepTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SleepTabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SleepTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
