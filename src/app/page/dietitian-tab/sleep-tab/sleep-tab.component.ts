import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { HttpService } from '../../../services/http.service';
import { MessageService } from 'primeng/api';
import { PrimeNGConfig } from 'primeng/api';

@Component({
  selector: 'app-sleep-tab',
  templateUrl: './sleep-tab.component.html',
  styleUrls: ['./sleep-tab.component.scss'],
  providers: [MessageService]
})
export class SleepTabComponent implements OnInit {
  ckContentData: any;
  name = "ng2-ckeditor";
  ckeConfig: any;
  mycontent: string = '';
  log: string = "";
  @ViewChild("myckeditor") ckeditor: any;
  rangeunits: number = 14;
  saveButton: boolean = false;
  previousButton: boolean = false;
  id: any;

  constructor(
    private route: ActivatedRoute, 
    private router: Router, 
    private http: HttpClient, 
    private httpService: HttpService,
    private messageService: MessageService,
    private primengConfig: PrimeNGConfig) {

  }

  ngOnInit() {
    this.primengConfig.ripple = true;
    // let getToken = JSON.parse(localStorage.getItem('testkit_id')!)
    // this.id = getToken.id
    this.id = localStorage.getItem('UserId')

    this.previousButton = true;
    this.getCKContent();

    this.ckeConfig = {
      allowedContent: true,
      extraPlugins:
        "easyimage,dialogui,dialog,a11yhelp,about,basicstyles,bidi,blockquote,clipboard," +
        "button,panelbutton,panel,floatpanel,colorbutton,colordialog,menu," +
        "contextmenu,dialogadvtab,div,elementspath,enterkey,entities,popup," +
        "filebrowser,find,fakeobjects,flash,floatingspace,listblock,richcombo," +
        "font,format,forms,horizontalrule,htmlwriter,iframe,image,indent," +
        "indentblock,indentlist,justify,link,list,liststyle,magicline," +
        "maximize,newpage,pagebreak,pastefromword,pastetext,preview,print," +
        "removeformat,resize,save,menubutton,scayt,selectall,showblocks," +
        "showborders,smiley,sourcearea,specialchar,stylescombo,tab,table," +
        "tabletools,templates,toolbar,undo,wsc,wysiwygarea"
    };
  }

  onEditorChange(event: any) {
    //console.log(event);
  }

  onChange(event: any): void {
    
    // console.log(event);
    // console.log(this.mycontent);
  }

  //Get ck editor details
  getCKContent(){
    let body = {
      examination_id: this.id,
      menu_id: 6
    }
    this.httpService.post("api/admin/dietitian-template", body).subscribe((data: any) =>{
      if(data.status == 1) {
        this.ckContentData = data;
        this.mycontent = this.ckContentData.template;
        //console.log(data)
      }
      else {
        this. errorFunction(data)
      }
    })
  }
  
 
 //next page navigation after form submission 
  onPageLoad(value: any){
    if(this.mycontent == ""){
      this.messageService.add({key: 'sleep', severity:'error', summary: 'Sleep Health', detail: 'Invalid Sleep Health!'});
    }
    if(this.mycontent != ""){
      let body = {
        examination_id: this.id,
        life_style: null,
        medical_history: null,
        dietary_report: null,
        bmi: null,
        job: null,
        sleep: this.mycontent,
        sexual_health: null,
        family_history: null,
        exercise: null,
        problem_area: null,
        submitted:0
      }
      this.httpService.post('api/admin/dietitian-store', body).subscribe((data: any) => {
        // console.log(data)
        if(data.status == 1){
          if(value == 4){
            this.router.navigate(["/page/dietitian/job"], { relativeTo: this.route }).then(() => {
              window.location.reload();
            });
          }
          if(value == 6){
            this.router.navigate(["/page/dietitian/sexual-health"], { relativeTo: this.route }).then(() => {
              window.location.reload();
            });
          }
          this.messageService.add({key: 'sleep', severity:'success', summary: 'Sleep Health', detail: 'Sleep health updated successfully!'});
        }else{
          this.messageService.add({key: 'sleep', severity:'error', summary: 'Sleep Health', detail: 'Invalid Sleep Health!'});
        }
      })
    }
    

  }

  //AddSleep Savedraft
  onAddSleep() {
    if(this.mycontent == ""){
      this.messageService.add({key: 'sleep', severity:'error', summary: 'Sleep Health', detail: 'Invalid Sleep Health!'});
    }
    if(this.mycontent != ""){
      let body = {
        examination_id: this.id,
        life_style: null,
        medical_history: null,
        dietary_report: null,
        bmi: null,
        job: null,
        sleep: this.mycontent,
        sexual_health: null,
        family_history: null,
        exercise: null,
        problem_area: null,
        submitted:0
      }
      this.httpService.post('api/admin/dietitian-store', body).subscribe((data: any) => {
        // console.log(data)
        if(data.status == 1){
          this.messageService.add({key: 'sleep', severity:'success', summary: 'Sleep Health', detail: 'Sleep health updated successfully!'});
        }else{
          this.messageService.add({key: 'sleep', severity:'error', summary: 'Sleep Health', detail: 'Invalid Sleep Health!'});
        }
      })
    }
    
  }

  errorFunction(data:any){
    console.log(data)
    var message = data.message.name
    if(data.message.name === 'TokenExpiredError' ) {
      localStorage.clear();
    this.router.navigate([`login`]);
    }
  }


}
