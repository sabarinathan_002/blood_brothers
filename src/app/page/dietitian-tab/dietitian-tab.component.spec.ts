import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DietitianTabComponent } from './dietitian-tab.component';

describe('DietitianTabComponent', () => {
  let component: DietitianTabComponent;
  let fixture: ComponentFixture<DietitianTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DietitianTabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DietitianTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
