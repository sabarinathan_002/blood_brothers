import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DietaryReportComponent } from './dietary-report.component';

describe('DietaryReportComponent', () => {
  let component: DietaryReportComponent;
  let fixture: ComponentFixture<DietaryReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DietaryReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DietaryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
