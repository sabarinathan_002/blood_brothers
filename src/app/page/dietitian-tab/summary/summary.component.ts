import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { HttpService } from '../../../services/http.service';
import { MessageService } from 'primeng/api';
import { PrimeNGConfig } from 'primeng/api';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss'],
  providers: [MessageService]
})
export class SummaryComponent implements OnInit {

  ckContentData: any;
  name = "ng2-ckeditor";
  ckeConfig: any;
  mycontent: string = '';
  log: string = "";
  @ViewChild("myckeditor") ckeditor: any;
  rangeunits: number = 14;
  saveButton: boolean = false;
  previousButton: boolean = false;
  id: any;
  summary_problem: Array<any> = []
  examination_id:any;
  dietitianData:any;
  displayBasic:boolean = false;

  lifestyle:any;
  medical_history:any;
  ditery:any;
  bmi:any;
  job:any;
  sleep:any;
  sexual_health:any;
  family_history:any;
  excersie:any;
  summary_problems:any;
  username:any;

  constructor(
    private route: ActivatedRoute, 
    private router: Router, 
    private http: HttpClient, 
    private httpService: HttpService,
    private messageService: MessageService,
    private primengConfig: PrimeNGConfig, protected _sanitizer: DomSanitizer) {

  }

  ngOnInit() {
    this.examination_id = localStorage.getItem('UserId')
    this.primengConfig.ripple = true;
    let getToken = JSON.parse(localStorage.getItem('testkit_id')!)
    this.id = getToken.id

    this.previousButton = true;
    // this.getCKContent();
    // this.getBloodResults();
    this.getdietitianData();
    this.ckeConfig = {
      extraPlugins:
        "easyimage,dialogui,dialog,a11yhelp,about,basicstyles,bidi,blockquote,clipboard," +
        "button,panelbutton,panel,floatpanel,colorbutton,colordialog,menu," +
        "contextmenu,dialogadvtab,div,elementspath,enterkey,entities,popup," +
        "filebrowser,find,fakeobjects,flash,floatingspace,listblock,richcombo," +
        "font,format,forms,horizontalrule,htmlwriter,iframe,image,indent," +
        "indentblock,indentlist,justify,link,list,liststyle,magicline," +
        "maximize,newpage,pagebreak,pastefromword,pastetext,preview,print," +
        "removeformat,resize,save,menubutton,scayt,selectall,showblocks," +
        "showborders,smiley,sourcearea,specialchar,stylescombo,tab,table," +
        "tabletools,templates,toolbar,undo,wsc,wysiwygarea"
    };

  }

  onEditorChange(event: any) {
   // console.log(event);
  }

  onChange(event: any): void {
    
    // console.log(event);
    // console.log(this.mycontent);
  }

  getCKContent(){
    let body = {
      examination_id: this.id,
      menu_id: 10
    }
    this.httpService.post("api/admin/dietitian-template", body).subscribe((data: any) =>{
      this.ckContentData = data;
      this.mycontent = this.ckContentData.template;
      //console.log(data)
    })
  }


  onPageLoad(value: any){
    if(value == 8){
      this.router.navigate(["/page/dietitian/exercise"], { relativeTo: this.route });
    }
  }

  onAddSummaryProblem() {
    if(this.mycontent == ""){
      this.messageService.add({key: 'summary', severity:'error', summary: 'Summary Problem Areas', detail: 'Invalid Summary Problem Areas!'});
    }
    if(this.mycontent != ""){
      let body = {
        examination_id: this.id,
        life_style: null,
        medical_history: null,
        dietary_report: null,
        bmi: null,
        job: null,
        sleep: null,
        sexual_health: null,
        family_history: null,
        exercise: null,
        problem_area: this.mycontent,
        submitted:1
      }
      this.httpService.post('api/admin/dietitian-store', body).subscribe((data: any) => {
        if(data.status == 1){
          // this.messageService.add({key: 'summary', severity:'success', summary: 'Success', detail: 'Saved successfully!'});
          this.displayBasic = true
        }else{
          this.messageService.add({key: 'summary', severity:'error', summary: 'Info', detail: 'Invalid details Areas!'});
        }
      })
    }
    
  }

  getBloodResults(){
    this.httpService.getDietitianDatas("api/admin/abnormal/detail/"+this.examination_id).subscribe((userData: any) => { 
      console.log('check4',userData.data)
      this.summary_problem = userData.data
    } )
  }

  getdietitianData(){
    this.httpService.get("api/admin/dietitian-result/"+this.examination_id).subscribe((data:any) => {
      console.log(data)
      this.dietitianData = data

      this.lifestyle = this._sanitizer.bypassSecurityTrustHtml(data.life_style)
      this.medical_history = this._sanitizer.bypassSecurityTrustHtml(data.medical_history)
      this.ditery = this._sanitizer.bypassSecurityTrustHtml(data.dietary_report)
      this.bmi = this._sanitizer.bypassSecurityTrustHtml(data.bmi)
      this.job = this._sanitizer.bypassSecurityTrustHtml(data.job)
      this.sleep = this._sanitizer.bypassSecurityTrustHtml(data.sleep)
      this.sexual_health = this._sanitizer.bypassSecurityTrustHtml(data.sexual_health)
      this.family_history = this._sanitizer.bypassSecurityTrustHtml(data.family_history)
      this.excersie = this._sanitizer.bypassSecurityTrustHtml(data.exercise)
      this.summary_problems = this._sanitizer.bypassSecurityTrustHtml(data.problem_area)
    })
  }

  close() {
    this.displayBasic = false
   }
 
   Open(){
     // this.router.navigate([`search`]);
     this.displayBasic = false
     this.router.navigate([`/page/dietitian/Summary-problem-areas`]).then(() => {
      window.location.reload();
    });
 }


}
