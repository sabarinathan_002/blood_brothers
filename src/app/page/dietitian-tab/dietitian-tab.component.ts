import { Component, OnInit, ViewChild, ViewEncapsulation  } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpService } from '../../services/http.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-dietitian-tab',
  templateUrl: './dietitian-tab.component.html',
  styleUrls: ['./dietitian-tab.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DietitianTabComponent implements OnInit {
  examination_id:any;
  getUserData: any;
  personalDetail: any;
  medicalHistory: any;
  lifestyleFactore: any;
  diet: any;
  summaryData: any;
  summaryDietitianData: any;
  summaryName: any;
  summaryTemplateNote: any;
  summaryTemplateChart: any;
  diabetes_timeline:Array<any> = []
  diabetes_timeline_data:any;
  sidebar_details:Array<any> =  [];
  completed_id:any;
  sidebar_id:any;


  constructor(
    private http: HttpClient, 
    private httpService: HttpService, 
    protected _sanitizer: DomSanitizer,
    private router: Router, private acRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.examination_id  = localStorage.getItem('UserId')
    this.sidebar_id = localStorage.getItem('side-bar-id')
    this.completed_id = localStorage.getItem('completed')
    this.getPattientDatas();
    this.getSummaryData();
    this.getSidebarData();
  }

  getSidebarData(){
    this.httpService.get('api/admin/dietitian-menu/'+this.sidebar_id).subscribe((response:any) => {
        console.log('SideBar - Details',response)
        if(response.status == 1){
          for (let i = 0; i < response.data.length; i++) { 
            var loopdata = response.data[i]
            var body;
            if(loopdata.menu_id == 1){
              body = {
                url:'lifestyle',
                name:"Lifestyle Factors",
                update: loopdata.updated
              }
            }
            else if(loopdata.menu_id == 2){
              body = {
                url:'medical-history',
                name:"Medical History",
                update: loopdata.updated
              }
            }
            else if(loopdata.menu_id == 3){
              body = {
                url:'dietary-report',
                name:"Dietary Report",
                update: loopdata.updated
              }
            }
            else if(loopdata.menu_id == 4){
              body = {
                url:'bmi',
                name:"Body Mass Index (BMI)",
                update: loopdata.updated
              }
            }
            else if(loopdata.menu_id == 5){
              body = {
                url:'job',
                name:"Job",
                update: loopdata.updated
              }
            }
            else if(loopdata.menu_id == 6){
              body = {
                url:'sleep',
                name:"Sleep",
                update: loopdata.updated
              }
            }
            else if(loopdata.menu_id == 7){
              body = {
                url:'sexual-health',
                name:"Sexual Health",
                update: loopdata.updated
              }
            }
            else if(loopdata.menu_id == 8){
              body = {
                url:'family-history',
                name:"Family History",
                update: loopdata.updated
              }
            }
            else if(loopdata.menu_id == 9){
              body = {
                url:'exercise',
                name:"Exercise",
                update: loopdata.updated
              }
            }
            else if(loopdata.menu_id == 10){
              body = {
                url:'summary',
                name:"Follow Up",
                update: loopdata.updated
              }
            }
            this.sidebar_details.push(body)
          }
        }
        else {
          this.errorFunction(response)
        }
        console.log('dietitian',this.sidebar_details)
    })
    
  }

  getPattientDatas(){
    this.httpService.getDietitianDatas("api/admin/survey/detail/"+this.examination_id).subscribe((data: any) => {
      console.log(data)
      this.personalDetail = data.personal_detail;
      this.medicalHistory = data.medical_history;
      this.lifestyleFactore = data.lifestyle_factore;
      this.diet = data.diet;
      
    })
  }

  getSummaryData(){
    let userData = localStorage.getItem('testkit_id')!;
    this.getUserData = JSON.parse(userData);
    this.httpService.getDietitianDatas("api/admin/abnormal/detail/"+this.examination_id).subscribe((userData: any) => {
      console.log(userData.timeline)
      this.diabetes_timeline = userData.timeline
        var data1 = this.diabetes_timeline.filter((a) => {
          return a.selected == 1
        })
        this.diabetes_timeline_data = data1[0]
        console.log('check3',this.diabetes_timeline_data)
    // const byId = userData.data.filter((res: any) => res.test_id === this.getUserData.id);
    this.summaryData = userData.data
    console.log('check4',this.summaryData)
    })
  }


  onTabChange(event: any){
    if(event !== null){
      document.getElementsByClassName("active-tab-content")[0].classList.remove("active-tab-content");
      document.getElementById(event)?.classList.add('active-tab-content')
      document.getElementsByClassName("active-nav-tab")[0].classList.remove("active-nav-tab");
      document.getElementsByClassName("nav-"+event)[0].classList.add("active-nav-tab");
    }
    
  }

  errorFunction(data: any) {
    console.log(data)
    var message = data.message.name
    if (data.message.name === 'TokenExpiredError') {
      localStorage.clear();
      this.router.navigate([`login`]);
    }
  }
}
