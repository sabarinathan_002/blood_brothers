import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryProblemAreasComponent } from './summary-problem-areas.component';

describe('SummaryProblemAreasComponent', () => {
  let component: SummaryProblemAreasComponent;
  let fixture: ComponentFixture<SummaryProblemAreasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SummaryProblemAreasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryProblemAreasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
