import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule  } from "@angular/forms";

import { ThemeModule } from '../../theme/theme.module';
import { DietitianTabRoutingModule } from './dietitian-tab-routing.module';
import { DietitianTabComponent } from './dietitian-tab.component';
import { LifestyleComponent } from './lifestyle/lifestyle.component';
import { CKEditorModule } from 'ckeditor4-angular';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSliderModule} from '@angular/material/slider';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { MedicalHistoryComponent } from './medical-history/medical-history.component';
import { DietaryReportComponent } from './dietary-report/dietary-report.component';
import { BodyMassIndexComponent } from './body-mass-index/body-mass-index.component';
import { JobTabComponent } from './job-tab/job-tab.component';
import { SleepTabComponent } from './sleep-tab/sleep-tab.component';
import { SexualHealthComponent } from './sexual-health/sexual-health.component';
import { FamilyHistoryComponent } from './family-history/family-history.component';
import { ExerciseComponent } from './exercise/exercise.component';
import { SummaryProblemAreasComponent } from './summary-problem-areas/summary-problem-areas.component';
import { SafeHtmlPipe } from 'src/app/custom-pipe/safe-html.pipe';

import {MatTabsModule} from '@angular/material/tabs';
import { ButtonModule } from 'primeng/button';
import { ToastModule } from 'primeng/toast';
import { RippleModule } from 'primeng/ripple';
import { DietitianHeaderComponent } from './dietitian-header/dietitian-header.component';

import {
  NbActionsModule,
  NbCardModule,
  NbInputModule,
  NbDialogModule,
  NbButtonModule,
  NbIconModule,
  NbToggleModule
} from "@nebular/theme";
import {DialogModule} from 'primeng/dialog';
import { SummaryComponent } from './summary/summary.component';
import { FollowUpComponent } from './follow-up/follow-up.component';

@NgModule({
  declarations: [
    DietitianTabComponent,
    LifestyleComponent,
    MedicalHistoryComponent,
    DietaryReportComponent,
    BodyMassIndexComponent,
    JobTabComponent,
    SleepTabComponent,
    SexualHealthComponent,
    FamilyHistoryComponent,
    ExerciseComponent,
    SummaryProblemAreasComponent,
    SafeHtmlPipe,
    DietitianHeaderComponent,
    SummaryComponent,
    FollowUpComponent,
    
  ],
  imports: [
    CommonModule,
    ThemeModule,
    DietitianTabRoutingModule,
    CKEditorModule,
    FormsModule,
    ReactiveFormsModule,
    MatSlideToggleModule,
    MatButtonToggleModule,
    MatSliderModule,
    MatTabsModule,
    ButtonModule,
    ToastModule,
    RippleModule,
    NbActionsModule,
  NbCardModule,
  NbInputModule,
  NbDialogModule.forRoot(),
  NbButtonModule,
  NbIconModule,
  NbToggleModule,
  DialogModule
  ]
})
export class DietitianTabModule {}
