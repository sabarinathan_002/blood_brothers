import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BodyMassIndexComponent } from './body-mass-index/body-mass-index.component';
import { DietaryReportComponent } from './dietary-report/dietary-report.component';
import { DietitianTabComponent } from './dietitian-tab.component';
import { ExerciseComponent } from './exercise/exercise.component';
import { FamilyHistoryComponent } from './family-history/family-history.component';
import { FollowUpComponent } from './follow-up/follow-up.component';
import { JobTabComponent } from './job-tab/job-tab.component';
import { LifestyleComponent } from './lifestyle/lifestyle.component';
import { MedicalHistoryComponent } from './medical-history/medical-history.component';
import { SexualHealthComponent } from './sexual-health/sexual-health.component';
import { SleepTabComponent } from './sleep-tab/sleep-tab.component';
import { SummaryProblemAreasComponent } from './summary-problem-areas/summary-problem-areas.component';
import { SummaryComponent } from './summary/summary.component';

const routes: Routes = [
  {
    path: '',
    component: DietitianTabComponent,
    children: [
      {
        path: 'lifestyle',
        component: LifestyleComponent,
      },
      {
        path: 'medical-history',
        component: MedicalHistoryComponent,
      },
      {
        path: 'dietary-report',
        component: DietaryReportComponent,
      },
      {
        path: 'bmi',
        component: BodyMassIndexComponent,
      },
      {
        path: 'job',
        component: JobTabComponent,
      },
      {
        path: 'sleep',
        component: SleepTabComponent,
      },
      {
        path: 'sexual-health',
        component: SexualHealthComponent,
      },
      {
        path: 'family-history',
        component: FamilyHistoryComponent,
      },
      {
        path: 'exercise',
        component: ExerciseComponent,
      },
      {
        path: 'summary',
        component: SummaryComponent,
      },
      {
        path: 'Summary-problem-areas',
        component: SummaryProblemAreasComponent,
      },
      {
        path: 'followup',
        component: FollowUpComponent,
      },
      
      { path: '', redirectTo: 'lifestyle', pathMatch: 'full' },
      { path: '**', redirectTo: 'lifestyle' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DietitianTabRoutingModule { }
