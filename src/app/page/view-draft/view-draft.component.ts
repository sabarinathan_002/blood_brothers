import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../services/http.service'
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-view-draft',
  templateUrl: './view-draft.component.html',
  styleUrls: ['./view-draft.component.scss']
})
export class ViewDraftComponent implements OnInit {

  constructor(private http: HttpService, public router: Router,
    private acRoute: ActivatedRoute, private location: Location) { }
  search: any = '';
  viewDraft_list: Array<any> = []
  roles: any;
  page_count: any = 10;
  total_item: any = 30;
  current_page: any = 1;

  ngOnInit(): void {
    localStorage.setItem('completed', '1')
    this.roles = localStorage.getItem('roles1')
    if (this.roles == 'ROLE_ADMIN') {
      this.getViewDraftList()
    }
    else {
      this.getViewDraftList1()
    }

  }

  searchFunction() {
    if (this.search.length >= 2) {
      if (this.roles == 'ROLE_ADMIN') {
        this.getViewDraftList()
      }
      else {
        this.getViewDraftList1()
      }
    }
    else if (this.search == " " || null || undefined) {
      if (this.roles == 'ROLE_ADMIN') {
        this.getViewDraftList()
      }
      else {
        this.getViewDraftList1()
      }
    }
  }

  //View draft list for consultant
  getViewDraftList() {
    var body = {
      search: this.search,
      pagecount:this.page_count,
      pagevalue:this.current_page
    }
    this.http.post('api/admin/consultant-draft', body).subscribe((data: any) => {
      this.total_item = data.data_count
      this.viewDraft_list = data.data
      this.viewDraft_list = this.viewDraft_list.reverse()
      // console.log(data.data,this.viewDraft_list)
    })
  }

  //View draft list for dietitian
  getViewDraftList1() {
    var body = {
      search: this.search,
      pagecount:this.page_count,
      pagevalue:this.current_page
    }
    this.http.post('api/admin/dietitian-draft', body).subscribe((data: any) => {
      this.total_item = data.data_count
      this.viewDraft_list = data.data
      this.viewDraft_list = this.viewDraft_list.reverse()
      // console.log('dietitian', data.data, this.viewDraft_list)
    })
  }

  //test kit-id click navigate for consultant
  testKitId(value: any) {
    localStorage.setItem('examination_id', value)
    this.headervalueSet(value)
  }

  //test kit-id click navigate for dietitian
  testKitId1(value: any, value1: any) {
    localStorage.setItem('UserId', value)
    localStorage.setItem('side-bar-id',value)
    this.searchfn(value1)
    setTimeout(() => {
      this.router.navigate(["/page/dietitian/lifestyle"])
    }, 750);

  }

  //View draft list search function
  searchfn(value: any) {
    let body = {
      testkit_id: value
    }
    this.http.post('api/admin/testkit/search', body).subscribe((data: any) => {
      JSON.stringify(localStorage.setItem('testkit_id', JSON.stringify(data)))
    })
  }

  //header value set function
  headervalueSet(id: any) {
    this.http.get('api/admin/patient/detail/' + id).subscribe((data: any) => {
      console.log('header-details', data)
      localStorage.setItem('testkit_id', JSON.stringify(data))
      this.router.navigate(['/page/tabs/cholesterol'])
      // const queryParams: Params = { id: 1, length: 1, title: 'Triglycerides' };
      // this.router.navigate(['/page/tabs/cholesterol'], {
      //   relativeTo: this.acRoute,
      //   queryParams: queryParams,
      //   queryParamsHandling: 'merge',
      // })
      // this.location.re
    })
  }

  paginatorChange(value:any){
    console.log(value)
    this.page_count = value.rows
    this.current_page = value.page + 1
    if(this.roles == 'ROLE_ADMIN') {
      this.getViewDraftList()
     }
     else {
        this.getViewDraftList1()
     }
  }

  errorFunction(data: any) {
    console.log(data)
    var message = data.message.name
    if (data.message.name === 'TokenExpiredError') {
      localStorage.clear();
      this.router.navigate([`login`]);
    }
  }


}
