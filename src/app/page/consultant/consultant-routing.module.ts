import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CholesterolComponent } from './cholesterol/cholesterol.component';
import { DiabetesTimelineComponent } from './diabetes-timeline/diabetes-timeline.component';
import { ConsultantComponent } from './consultant.component';
import { SummaryComponent } from './summary/summary.component';

const routes: Routes = [
  {
    path: '',
    component: ConsultantComponent,
    children: [
      {
        path: 'cholesterol',
        component: CholesterolComponent,
      },
      {
        path: 'diabetes-timeline',
        component: DiabetesTimelineComponent,
      },
      {
        path: 'summary',
        component: SummaryComponent,
      },
      { path: '', redirectTo: 'cholesterol', pathMatch: 'full' },
      { path: '**', redirectTo: 'cholesterol' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsultantRoutingModule { }
