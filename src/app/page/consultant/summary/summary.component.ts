import { Component, OnInit, TemplateRef } from '@angular/core';
import { HttpService } from '../../../services/http.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params, } from "@angular/router";
import { NbDialogService } from "@nebular/theme";

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {
  examination_id: any;
  summaryData: Array<any> = []
  diabetes_timeline: Array<any> = []
  diabetes_timeline_data: any;
  completed_id: any;

  constructor(private http: HttpService, protected _sanitizer: DomSanitizer,
    private activateroute: ActivatedRoute, private router: Router, private dialogService: NbDialogService) { }


  ngOnInit(): void {
    this.examination_id = localStorage.getItem('examination_id')
    // localStorage.setItem('completed','1')
    this.getSummaryData()
    // localStorage.setItem('completed','1')
    this.completed_id = localStorage.getItem('completed')
    
  }

  //get summary data
  getSummaryData() {
    this.http.get("api/admin/abnormal/detail/" + this.examination_id).subscribe((userData: any) => {
      console.log(userData)
      if (userData.status == 1) {
        console.log('check3', userData)
        
        this.diabetes_timeline = userData.timeline
        var data1 = this.diabetes_timeline.filter((a) => {
          return a.selected == 1
        })

        this.diabetes_timeline_data = data1[0]
        console.log(this.diabetes_timeline_data)
        // const byId = userData.data.filter((res: any) => res.test_id === this.getUserData.id);
        this.summaryData = userData.data
      } else {
        this.errorFunction(userData)
      }
    })

  }

  //Popup model open and close
  close(modelRef: any) {
    modelRef.close();
  }

  Open() {
    this.router.navigate([`search`]);
  }

  goSearchFunction(){
    this.router.navigate(['search'])
  }

  onSubmit(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog, { closeOnBackdropClick: false, });
  }

  errorFunction(data: any) {
    console.log(data)
    var message = data.message.name
    if (data.message.name === 'TokenExpiredError') {
      localStorage.clear();
      this.router.navigate([`login`]);
    }
  }

}
