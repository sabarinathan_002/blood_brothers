import { Component, OnInit, ViewChild, Renderer2 } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpService } from '../../../services/http.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { MessageService } from 'primeng/api';
import { PrimeNGConfig } from 'primeng/api';
import { Router, ActivatedRoute, Params, } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { SidebarComponent } from "../../../theme/sidebar/sidebar.component"


@Component({
  selector: 'app-cholesterol',
  templateUrl: './cholesterol.component.html',
  styleUrls: ['./cholesterol.component.scss'],
  providers: [MessageService, SidebarComponent]
})
export class CholesterolComponent implements OnInit {

  name = "ng2-ckeditor";
  ckeConfig: any;
  mycontent: string = '';
  log: string = "";
  @ViewChild("myckeditor") ckeditor: any;

  templatestring: any;
  selectedItem: any = '';
  templateArray: Array<any> = []
  range: any;
  videourl: any;
  abnormal: any;
  url_tilte: any;
  url_description: any;
  template: any;
  load_mycontent: any;
  id: any;
  header_name: any;
  array_length: any;
  sidebar_number: any;
  save_button: boolean = false;
  previous_button: boolean = false;
  sidebar_list: any;
  range_error: boolean = false;
  examination_id: any;
  min: any;
  max: any;

  constructor(private http: HttpService, protected _sanitizer: DomSanitizer,
    private messageService: MessageService, private primengConfig: PrimeNGConfig,
    private activateroute: ActivatedRoute, private router: Router,
    private spinner: NgxSpinnerService, private renderer: Renderer2,
    private sidebar: SidebarComponent) {
  }

  ngOnInit(): void {
    // window.location.reload();
    localStorage.setItem('completed','1')
    setTimeout(() => {

      var elem = this.renderer.selectRootElement('#myInput3');

      this.renderer.listen(elem, "focus", () => { console.log('focus') });

      this.renderer.listen(elem, "blur", () => { console.log('blur') });

      elem.focus();

    }, 1000);

    this.examination_id = localStorage.getItem('examination_id')
 
      
 

    setTimeout(() => {
      this.sidebar_list = JSON.parse(localStorage.getItem('sidebarList')!)
      

      this.activateroute.queryParams.subscribe((params) => {
        console.log('Cholestrol Prams',params.id)
        if (params.id == undefined || null || '' ) {
          console.log('params',this.sidebar_list[0].id)
          this.id =  this.sidebar_list[0].id
          this.sidebar_number = 1
        }
        else {
          this.id = params.id
          this.sidebar_number = params.length
        }
      })

      this.ckeditordetails()
      console.log(+this.sidebar_number, +this.sidebar_list.length - 1)
      if (+this.sidebar_number < +this.sidebar_list.length - 1) {
        this.save_button = false
      }
      else if (+this.sidebar_number == +this.sidebar_list.length - 1) {
        this.save_button = true
      }

      if (+this.sidebar_number == 1) {
        this.previous_button = false
      }
      else {
        this.previous_button = true
      }

      
    }, 800);


    



    this.primengConfig.ripple = true;

    this.ckeConfig = {
      allowedContent: true,
      extraPlugins:
        "easyimage,dialogui,dialog,a11yhelp,about,basicstyles,bidi,blockquote,clipboard," +
        "button,panelbutton,panel,floatpanel,colorbutton,colordialog,menu," +
        "contextmenu,dialogadvtab,div,elementspath,enterkey,entities,popup," +
        "filebrowser,find,fakeobjects,flash,floatingspace,listblock,richcombo," +
        "font,format,forms,horizontalrule,htmlwriter,iframe,image,indent," +
        "indentblock,indentlist,justify,link,list,liststyle,magicline," +
        "maximize,newpage,pagebreak,pastefromword,pastetext,preview,print," +
        "removeformat,resize,save,menubutton,scayt,selectall,showblocks," +
        "showborders,smiley,sourcearea,specialchar,stylescombo,tab,table," +
        "tabletools,templates,toolbar,undo,wsc,wysiwygarea"
    };

  }

  //ckeditor changes function tiggers
  onChange(event: any): void {
    console.log(event);
    console.log(this.mycontent);
  }

  //Load template
  loadTemplate() {
    this.load_mycontent = this.templatestring.note
    if (this.load_mycontent == undefined || null || '') {
      this.messageService.add({ key: 'bc', severity: 'info', summary: 'Info', detail: 'Please select template' });
    }
    else {
      this.mycontent = this.load_mycontent
      this.messageService.add({ key: 'bc', severity: 'success', summary: 'Success', detail: 'Template loaded Successfully' });
    }
  }

  // get Details
  ckeditordetails() {
    this.spinner.show();
    var body = {
      examination_id: this.examination_id,
      test_id: this.id,
    }
    console.log(body)
    this.http.post('api/admin/examination/detail', body).subscribe((response: any) => {
      console.log(response)
      this.range = response.value
      if (response.status == 1) {
        this.videourl = response.url_link
        this.abnormal = response.test_status == '1' ? true : false
        this.url_tilte = response.url_title
        this.url_description = response.url_desc
        this.header_name = response.test_name
        this.template = this._sanitizer.bypassSecurityTrustHtml(response.template)
        this.mycontent = response.notes
        this.templateArray = response.template_list
        var templatevalue = response.statustext
        this.min = response.min
        this.max = response.max
        var data = this.templateArray.filter((a) => templatevalue === a.title);
        this.templatestring = data[0]
      }
      else {
        this.errorFunction(response)
      }

      this.spinner.hide()
      // console.log(this.templateArray,this.templatestring)
    })
  }

  //abnormal slide toggle
  abnormalStatus(value: any) {
    console.log(value)
  }


  //Result value focus out
  resultFocusOut() {
    var body = {
      test_id: this.id,
      value: this.range
    }
    this.http.post('api/admin/examination/marker', body).subscribe((data: any) => {
      
      if (data.status == 1) {
        this.template = this._sanitizer.bypassSecurityTrustHtml(data.template)
        var templatevalue = data.statustext
        var data1 = this.templateArray.filter((a) => templatevalue === a.title);
        
        this.templatestring = data1[0]
        this.mycontent = data1[0].note
      }
      else {
        this.errorFunction(data)
      }
    })
  }

  //form refresh
  formRefresh(form: NgForm) {
    form.reset()
  }

  //form submit
  onCreate(form: NgForm, value: any) {
    console.log(value)
    var abnormalstatus = form.value.abnormal == true ? '1' : '0'
    this.spinner.show()

    if (form.valid) {
      var body = {
        examination_id: this.examination_id,
        test_id: this.id,
        notes: form.value.myckeditor,
        url_title: form.value.url_tilte,
        url_desc: form.value.url_description,
        url_link: form.value.videourl,
        value: +form.value.range,
        test_status: abnormalstatus,
      }
      this.http.post('api/admin/examination/store', body).subscribe((data: any) => {
        this.spinner.hide()
        this.messageService.add({ key: 'bc', severity: 'success', summary: 'Success', detail: 'Form Submitted Successfully' });
        if (value == 1) {
          var id = +this.id + 1
          var length = +this.sidebar_number + 1
          var name = this.sidebar_list.filter((value: any) => length == value.length)
          const queryParams: Params = { id: name[0].id, length: length, title: name[0].title };
          console.log(queryParams)
          this.router
            .navigateByUrl(`search`, { skipLocationChange: false })
            .then(() => {
              this.router.navigate(['/page/tabs/cholesterol'],
                {
                  relativeTo: this.activateroute,
                  queryParams: queryParams,
                  queryParamsHandling: 'merge',
                });
            });

        }
        else if (value == 2) {
          var length = +this.sidebar_number + 1
          var name = this.sidebar_list.filter((value: any) => length == value.length)
          const queryParams: Params = { id: 0, length: length, title: name[0].title };
          this.router
            .navigateByUrl(`search`, { skipLocationChange: false })
            .then(() => {
              this.router.navigate(['/page/tabs/diabetes-timeline'], {
                relativeTo: this.activateroute,
                queryParams: queryParams,
                queryParamsHandling: 'merge',
              });
            });

          this.spinner.hide()
        }
        else if (value == 3) {
          var length = +this.sidebar_number - 1
          var name = this.sidebar_list.filter((value: any) => length == value.length)
          console.log(name)
          console.log(name)
          const queryParams: Params = { id: name[0].id, length: length, title: name[0].title };
          this.router
            .navigateByUrl(`search`, { skipLocationChange: false })
            .then(() => {
              this.router.navigate(['/page/tabs/cholesterol'],
                {
                  relativeTo: this.activateroute,
                  queryParams: queryParams,
                  queryParamsHandling: 'merge',
                });
            });
        }
        else {
          this.ckeditordetails()
          this.spinner.hide()
        }
      })
      // window.location.reload();
      // this.sidebar.ngOnInit()
    }
    else {
      this.spinner.hide()
      this.messageService.add({ key: 'bc', severity: 'error', summary: 'Error', detail: 'Please fill all necessary details' });
    }
    // this.sidebar.sidebardetails()

  }

  // loadtemplate template
  dropdownOnchange(value: any) {
    // this.mycontent =value.note
    console.log(value.note)
  }

  errorFunction(data: any) {
    console.log(data)
    var message = data.message.name
    if (data.message.name === 'TokenExpiredError') {
      localStorage.clear();
      this.router.navigate([`login`]);
    }
  }


}
