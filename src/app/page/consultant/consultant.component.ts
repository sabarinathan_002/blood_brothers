import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-consultant',
  template: `
  <app-header></app-header>
  <app-layout>
    <router-outlet></router-outlet>
  </app-layout>
`
})
export class ConsultantComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
