import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DiabetesTimelineComponent } from './diabetes-timeline.component';

describe('DiabetesTimelineComponent', () => {
  let component: DiabetesTimelineComponent;
  let fixture: ComponentFixture<DiabetesTimelineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DiabetesTimelineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DiabetesTimelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
