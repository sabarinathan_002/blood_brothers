import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute, Params, } from "@angular/router";
import {HttpService} from '../../../services/http.service';
import { MessageService } from 'primeng/api';
import { PrimeNGConfig } from 'primeng/api';
import { NbDialogService } from "@nebular/theme";

@Component({
  selector: 'app-diabetes-timeline',
  templateUrl: './diabetes-timeline.component.html',
  styleUrls: ['./diabetes-timeline.component.scss'],
  providers: [MessageService]
})
export class DiabetesTimelineComponent implements OnInit {

  header_name: any;
  diabetesvalue:any;
  diabetesArray:Array<any> = [];
  id:any;
  examination_id:any;

  constructor(private activateroute: ActivatedRoute, private router: Router,
    private http: HttpService, private primengConfig: PrimeNGConfig,  private acRoute: ActivatedRoute,
    private messageService: MessageService, private dialogService: NbDialogService) { }

  ngOnInit(): void {
    this.examination_id = localStorage.getItem('examination_id')
    localStorage.setItem('completed','1')
    console.log(this.examination_id)
    this.primengConfig.ripple = true;
    this.activateroute.queryParams.subscribe((params) => {
      console.log('params', params)
      this.header_name = params.title
      this.id = params.id
    })
    this.diabetesDropdown()
    this.getDropdownValue()
  }

  //selected diabetes timeline 
  diabetesDropdown(){
    this.http.get('api/admin/timeline/list/'+this.examination_id).subscribe((data:any) => {
      if(data.status == 1){
      this.diabetesArray = data.data
      var data1 = this.diabetesArray.filter((a) => {
        return a.selected == 1
      })
      this.diabetesvalue = data1[0]
      console.log( this.diabetesvalue)
      console.log(this.diabetesArray )
      }
      else{
        this.errorFunction(data)
      }
    })
  }

  //get Diabetes timeline Dropdown
  getDropdownValue(){
    this.http.get('api/user/diabetes-result/'+this.examination_id).subscribe((data:any) => {
      console.log(data)
    })
  }

  //close modal
  close(modelRef:any) {
    modelRef.close();
  }

  //open modal
  Open(modelRef:any){
    // this.router.navigate([`search`]);
    modelRef.close();
    const queryParams: Params = {title: 'Summary' };
    localStorage.setItem('completed','1')
    this.router
    .navigateByUrl(`search`, { skipLocationChange: false })
    .then(() => {
      this.router.navigate(['/page/tabs/summary'], {
        relativeTo: this.acRoute,
        queryParams: queryParams,
        queryParamsHandling: 'merge',
      })
    });
      
  }

  //save and finalise
  onsubmit1(){
    if(this.diabetesvalue == undefined || null || '') {
      this.messageService.add({ key: 'bc', severity: 'error', summary: 'Error', detail: 'Please select dropdown value' });
    }
    else{
      
      var body ={
        examination_id: this.examination_id,
        timeline_id: this.diabetesvalue.id,
        submitted:0
      }
      console.log(this.examination_id)
      this.http.post('api/admin/timeline/store', body).subscribe((data) => {
          console.log(data)
          this.diabetesvalue = []
          this.messageService.add({ key: 'bc', severity: 'sucess', summary: 'Success', detail: 'Data Saved successfully' });
      })
    }
  }

  //save draft
  onsubmit(dialog: TemplateRef<any>){
    if(this.diabetesvalue == undefined || null || '') {
      this.messageService.add({ key: 'bc', severity: 'error', summary: 'Error', detail: 'Please select dropdown value' });
    }
    else{
      console.log(this.diabetesvalue.id)
      var body ={
        examination_id: this.examination_id,
        timeline_id: this.diabetesvalue.id,
        submitted:1
      }
      this.http.post('api/admin/timeline/store', body).subscribe((data) => {
          console.log(data)
          this.diabetesvalue = []
          this.dialogService.open(dialog , { closeOnBackdropClick: false});
      })
    }
  }

  errorFunction(data:any){
    console.log(data)
    var message = data.message.name
    if(data.message.name === 'TokenExpiredError' ) {
      localStorage.clear();
    this.router.navigate([`login`]);
    }
  }

 
}
