import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



import { RouterModule } from '@angular/router';
import {DropdownModule} from 'primeng/dropdown';


import { CholesterolComponent } from './cholesterol/cholesterol.component';
import { DiabetesTimelineComponent } from './diabetes-timeline/diabetes-timeline.component';
import { ThemeModule } from '../../theme/theme.module';
import { ConsultantRoutingModule } from './consultant-routing.module';
import { ConsultantComponent } from './consultant.component';

import { CKEditorModule } from 'ckeditor4-angular';
import { FormsModule } from "@angular/forms";
import { NbSelectModule } from '@nebular/theme'
import { ButtonModule } from 'primeng/button';
import { ToastModule } from 'primeng/toast';
import { RippleModule } from 'primeng/ripple';
import { NgxSpinnerModule } from "ngx-spinner";
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {
  NbActionsModule,
  NbCardModule,
  NbInputModule,
  NbDialogModule,
  NbButtonModule,
  NbIconModule,
  NbToggleModule
} from "@nebular/theme";
import { TextMaskModule } from 'angular2-text-mask';
import { SummaryComponent } from './summary/summary.component';
import {SafeHtml1Pipe} from '../../custom-pipe/safe-html1.pipe'


@NgModule({
  declarations: [
    ConsultantComponent,
    CholesterolComponent,
    DiabetesTimelineComponent,
    SummaryComponent,
    SafeHtml1Pipe,
  ],
  imports: [
    CommonModule,
    ConsultantRoutingModule,
    CKEditorModule,
    FormsModule,
    NbSelectModule,
    ButtonModule,
    ToastModule,
    RippleModule,
    RouterModule,
    NgxSpinnerModule,
    NbActionsModule,
    NbCardModule,
    NbInputModule,
    NbDialogModule.forRoot(),
    NbButtonModule,
    NbIconModule,
    ThemeModule,
    NbToggleModule,
    MatSlideToggleModule,
    MatButtonToggleModule,
    TextMaskModule,
    DropdownModule
  ]
})
export class ConsultantModule { }
