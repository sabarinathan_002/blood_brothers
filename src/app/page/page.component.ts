import { Component, OnInit } from '@angular/core';
import { HttpService } from '../services/http.service';
import { MENU_ITEMS } from "./page-menu";
import { Router, ActivatedRoute } from "@angular/router";


@Component({
  selector: 'app-page',
  styleUrls: ['./page.component.scss'],
  template: `
    <router-outlet></router-outlet>
`
})
export class PageComponent implements OnInit {
  menu = MENU_ITEMS;


  // <app-layout>
  //   <router-outlet></router-outlet>
  // </app-layout>



  constructor(private http: HttpService, private router: Router) {

  }

  ngOnInit(): void {

  }

}
