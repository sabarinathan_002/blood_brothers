import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompletedWorkComponent } from './completed-work/completed-work.component';
import { PageComponent } from './page.component';
import { ViewDraftComponent } from './view-draft/view-draft.component';
import { WorkListComponent } from './work-list/work-list.component';

const routes: Routes = [
  {
    path: '',
    component: PageComponent,
    children: [
      {
        path: 'tabs',
        loadChildren: () =>
          import('../page/consultant/consultant.module')
          .then((m) => m.ConsultantModule),
      },
      {
        path: 'dietitian',
        loadChildren: () =>
          import('../page/dietitian-tab/dietitian-tab.module')
          .then((m) => m.DietitianTabModule),
      },
      {
        path: 'viewdraft', component: ViewDraftComponent,
      },
      {
        path: 'worklist', component: WorkListComponent,
      },
      {
        path: 'completedlist', component: CompletedWorkComponent,
      },
      { path: '', redirectTo: 'tabs', pathMatch: 'full' },
      { path: '**', redirectTo: 'tabs' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageRoutingModule { }
