import { Component, OnInit } from '@angular/core';
import { HttpService} from '../../services/http.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-completed-work',
  templateUrl: './completed-work.component.html',
  styleUrls: ['./completed-work.component.scss']
})
export class CompletedWorkComponent implements OnInit {

  search: any = ''
  roles:any;
  complete_List : Array<any> = []
  page_count:any = 10;
  total_item:any;
  current_page:any = 1;

  constructor(private http: HttpService, public router: Router, 
    private acRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.roles = localStorage.getItem('roles1')
    localStorage.setItem('completed','1')
    if(this.roles == 'ROLE_ADMIN') {
      this.getCompletedListconsultant()
     }
     else {
        this.getCompletedListAdmin()
     }
    
  }

  searchFunction(){
    if (this.search.length >= 2) {
      if(this.roles == 'ROLE_ADMIN') {
        this.getCompletedListconsultant()
       }
       else {
        this.getCompletedListAdmin()
       }
    }
    else if(this.search == " " || null || undefined){ 
      if(this.roles == 'ROLE_ADMIN') {
        this.getCompletedListconsultant()
       }
       else {
        this.getCompletedListAdmin()
       }
    }
  }

  getCompletedListconsultant(){
    var body = {
      search: this.search, 
      pagecount:this.page_count,
      pagevalue:this.current_page
    }
    console.log(body)
    this.http.post('api/admin/consultant-completed', body).subscribe((data:any) => {
      if(data.status == 1){
        this.total_item = data.data_count
        this.complete_List = data.data
        // this.complete_List = this.complete_List.reverse()
      }
      else if(data.status == 0){
        this.errorFunction(data)
      }
    })
  }

  getCompletedListAdmin(){
    var body = {
      search: this.search, 
      pagecount:this.page_count,
      pagevalue:this.current_page
    }
    console.log(body)
    this.http.post('api/admin/dietitian-completed', body).subscribe((data:any) => {
      if(data.status == 1){
        this.total_item = data.data_count
        this.complete_List = data.data
        // this.complete_List = this.complete_List.reverse()
      }
      else if(data.status == 0){
        this.errorFunction(data)
      }
    })
  }

  //test kit-id click navigate for consultant
  testKitId(value:any){
    localStorage.setItem('examination_id', value)
    this.headervalueSet(value)
    // console.log('IdCheck',localStorage.getItem('examination_id'))
  }

   //test kit-id click navigate for dietitian
  testKitId1(value:any, value1:any){
    localStorage.setItem('UserId',value)
    localStorage.setItem('side-bar-id',value)
    this.searchfn(value1)
    setTimeout(() => {
      localStorage.setItem('completed','2')
      this.router.navigate([`/page/dietitian/Summary-problem-areas`])
    }, 750);
    
  }


  searchfn(value: any) {
    let body = {
      testkit_id: value
    }
    this.http.post('api/admin/testkit/search', body).subscribe((data: any) => {
      JSON.stringify(localStorage.setItem('testkit_id', JSON.stringify(data)))
    })
  }

  headervalueSet(id:any){
    this.http.get('api/admin/patient/detail/'+id).subscribe((data:any) => {
      localStorage.setItem('completed','2')
      localStorage.setItem('testkit_id', JSON.stringify(data))
      // const queryParams: Params = { id: 1,length: 1, title: 'Triglycerides' };
      // this.router.navigate(['/page/tabs/cholesterol'], {
      //       relativeTo: this.acRoute,
      //       queryParams: queryParams,
      //       queryParamsHandling: 'merge',
      //     })
      
      this.router.navigate(['/page/tabs/summary'])
    })
  }

  paginatorChange(value:any){
    console.log(value)
    this.page_count = value.rows
    this.current_page = value.page + 1
    if(this.roles == 'ROLE_ADMIN') {
      this.getCompletedListconsultant()
     }
     else {
        this.getCompletedListAdmin()
     }
  }

  errorFunction(data:any){
    console.log(data)
    var message = data.message.name
    if(data.message.name === 'TokenExpiredError' ) {
      localStorage.clear();
    this.router.navigate([`login`]);
    }
  }

}
