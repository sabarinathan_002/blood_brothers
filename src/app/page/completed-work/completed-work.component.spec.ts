import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedWorkComponent } from './completed-work.component';

describe('CompletedWorkComponent', () => {
  let component: CompletedWorkComponent;
  let fixture: ComponentFixture<CompletedWorkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompletedWorkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedWorkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
