import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  id: any;
  sidebar: Array<any> = [];
  itemsArray: Array<any> = []
  items: Array<any> = [];

  arrayLength: any = 1;
  counter: any = 1

  currentParam: any;
  currentPage: any;

  completed_id: any;

  constructor(private http: HttpService, private router: Router, private acRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = localStorage.getItem('sidebar_id')
    this.sidebardetails()
   
    this.acRoute.queryParams.subscribe(queryParams => {
      console.log('queryparam', queryParams)
      
      if (queryParams.title == undefined || null || '' ) {
        // this.currentParam = this.itemsArray[0].title
        console.log(this.currentParam)
      }
      else {
        this.currentParam = queryParams.title;
      }
      
   
      

    });

    this.completed_id = localStorage.getItem('completed')
  }

  //sidebar click navigation 
  goToPage(item: any) {
    console.log(this.completed_id)
    if (this.completed_id != 2) {
      this.currentPage = item;
      if (item.title == 'Diabetes Timeline') {
        const queryParams: Params = { id: item.id, length: item.length, title: item.title };
        this.router.navigate(['/page/tabs/diabetes-timeline'], {
          relativeTo: this.acRoute,
          queryParams: queryParams,
          queryParamsHandling: 'merge',
        })
      }
      else if (item.title == 'Summary') {
        const queryParams: Params = { title: item.title };
        this.router.navigate(['/page/tabs/summary'], {
          relativeTo: this.acRoute,
          queryParams: queryParams,
          queryParamsHandling: 'merge',
        })
      }
      else {
        const queryParams: Params = { id: item.id, length: item.length, title: item.title };
        this.router
          .navigateByUrl("/page/tabs/diabetes-timeline", { skipLocationChange: false })
          .then(() => {
            this.router.navigate(['/page/tabs/cholesterol'],
              {
                relativeTo: this.acRoute,
                queryParams: queryParams,
                queryParamsHandling: 'merge', // remove to replace all query params by provided
              });
          });
      }
    }
  }

  //sidebar details for consultant
  sidebardetails() {
    this.http.get('api/admin/examination/list/' + this.id).subscribe((response: any) => {
     
      if (response.status == 1) {
        for (let i = 0; i < response.data.length; i++) {
          var data = response.data[i]
          var body;
          if (i == response.data.length - 1) {

            body = {
              title: data.test_name,
              id: data.test_id,
              length: this.arrayLength,
              update: data.updated
            }
          }
          else {
            body = {
              title: data.test_name,
              id: data.test_id,
              length: this.arrayLength,
              update: data.updated
            }
          }
          this.itemsArray.push(body)
          this.arrayLength = this.arrayLength + 1
        }
        // var body1 = {
        //   title: 'Summary',
        // }
        // this.itemsArray.push(body1)
        this.items = this.itemsArray
        this.acRoute.queryParams.subscribe(queryParams => {
          console.log('queryParams within sidebar')
          if (queryParams.title == undefined || null || '' ) {
            var href = this.router.url;
            if (href == '/page/tabs/summary') {
              this.currentParam = ""
            }
            else{
              this.currentParam = this.itemsArray[0].title
            }
           
          }
          console.log(this.currentParam)
        }) 
       
        localStorage.setItem('sidebarList', JSON.stringify(this.itemsArray))
        console.log('1.2.3.4.5', this.itemsArray)
      }
      else {
        this.errorFunction(response)
      }
      // localStorage.setItem('examination_id', response.examination_id)

    })
  }

  errorFunction(data: any) {
    console.log(data)
    var message = data.message.name
    if (data.message.name === 'TokenExpiredError') {
      localStorage.clear();
      this.router.navigate([`login`]);
    }
  }


}
