import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout/layout.component';
import { RouterModule } from '@angular/router';

import { NbEvaIconsModule } from '@nebular/eva-icons';
import {
  NbThemeModule, NbLayoutModule,
  NbActionsModule, NbContextMenuModule, NbSidebarModule, NbMenuModule
} from '@nebular/theme';
import { MatSidenavModule } from '@angular/material/sidenav';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header/header.component'

const COMPONENTS = [
  LayoutComponent,SidebarComponent,HeaderComponent
]



@NgModule({
  declarations: [
    ...COMPONENTS,
  ],
  exports: [CommonModule, ...COMPONENTS],
  imports: [
    CommonModule,
    NbThemeModule.forRoot({ name: 'default' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbActionsModule,
    NbContextMenuModule,
    MatSidenavModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    RouterModule
  ]
})
export class ThemeModule { }
