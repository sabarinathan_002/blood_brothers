import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, } from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  setheader: boolean = false;
  completed_id:any;

  constructor(private acRoute: ActivatedRoute, private router: Router) {
    this.href = this.router.url;
    console.log(this.href)
    if (this.href == '/page/viewdraft') {
      this.setheader = false
    }
    else if (this.href == '/page/worklist') {
      this.setheader = false
    }
    else {
      this.setheader = true
    }

    console.log(this.setheader)
  }

  subheader_details: any;
  roles: any;
  href: any;
  age:any;

  ngOnInit(): void {
    this.href = this.router.url;
    var data1 = localStorage.getItem('testkit_id')!
    this.roles = localStorage.getItem('roles1')
    console.log(JSON.parse(data1))
    this.subheader_details = JSON.parse(data1)
    var dob = new Date(this.subheader_details.dob)
    var currentYear = new Date()
    this.age = currentYear.getUTCFullYear() - dob.getUTCFullYear()
    this.completed_id=localStorage.getItem('completed')
  }

  logout() {
    localStorage.clear();
    this.router.navigate([`login`]);
  }

  headerLogo() {
    const queryParams: Params = { id: 1, length: 1, title: 'Triglycerides' };
    this.router.navigate(['search'])
  }

  // idSet(){
  //   localStorage.setItem('completed','1')
  // }

}
