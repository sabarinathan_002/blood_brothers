import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { NbButtonModule, NbFormFieldModule, NbIconModule, NbInputModule } from '@nebular/theme';
import { FormsModule} from "@angular/forms";
import { NgxSpinnerModule } from "ngx-spinner";



@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
     NbButtonModule, 
     NbFormFieldModule, 
     NbIconModule, 
     NbInputModule,
     NgxSpinnerModule
  ]
})
export class LoginModule { }
