import { Component, OnInit } from '@angular/core';
import { HttpService } from '../services/http.service';
import { NgxSpinnerService } from "ngx-spinner";
import { NgForm } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [MessageService]
})
export class LoginComponent implements OnInit {
  email: any;
  password: any;
  submitted: boolean = false;
  error_shown : boolean = false;
  error_message:any;
  public passwordTextType: boolean = false;
  constructor(private http: HttpService, public router: Router, private spinner: NgxSpinnerService, private messageService: MessageService) { }

  ngOnInit(): void {
  }

  login(form: NgForm) {
    this.submitted = true
    if (form.valid) {
      this.spinner.show();
      let url = 'api/auth/signin';
      let data = {
        email: form.value.email,
        password: form.value.password,
      }
      this.http.login(url, data).subscribe((response: any) => {
        if (response.status === 1) {
          console.log(response)
          console.log(response.roles.toString())
          localStorage.setItem('roles1', response.roles.toString())
          localStorage.setItem('access_token', response.accessToken);
          this.spinner.hide()
          this.messageService.add({ key: 'bc', severity: 'success', summary: 'Success', detail: response.message });
          if (response.roles.toString() == 'ROLE_DIETITIAN') {
            this.router.navigate(['/page/worklist'])
          }
          else {
            this.router.navigate([`search`]);
          }
        } else if (response.status === 0) {
          this.error_shown = true;
          this.error_message = response.message
          // alert(response.message);
          this.spinner.hide()
          
          this.messageService.add({ key: 'bc', severity: 'error', summary: 'Error', detail: response.message });
        }
       this.submitted = false
      });
    }

  }

  formonChange(){
    this.error_shown = false
  }

  formRefresh(form: NgForm) {
    form.reset()
  }

  togglePasswordTextType() {
    this.passwordTextType = !this.passwordTextType;
  }

}
