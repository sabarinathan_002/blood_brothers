import { Component, OnInit } from '@angular/core';
import { HttpService } from '../services/http.service';
import { NgxSpinnerService } from "ngx-spinner";
import { NgForm } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  providers: [MessageService]
})
export class SearchComponent implements OnInit {

  testkit: any;
  testkit_data: any = null;
  testKit_error: boolean = false;
  completed_status: any;

  public mask = {
    guide: true,
    showMask: false,
    mask: [/\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  };

  constructor(private http: HttpService, public router: Router,
    private spinner: NgxSpinnerService, private messageService: MessageService, private acRoute: ActivatedRoute) { }

  ngOnInit(): void {
  }

  //open bloodresults pages
  resultsPageOpen() {
    var role = localStorage.getItem('roles1')
    if (role == 'ROLE_ADMIN') {
      if (this.completed_status == 1) {
        localStorage.setItem('completed','2')
        this.router.navigate(['/page/tabs/summary'])
      }
      else {
        // const queryParams: Params = { id: '26397080-811e-4ccd-aafe-d043d965ee30', length: 1, title: 'Triglycerides' };
        // this.router.navigate(['/page/tabs/cholesterol'], {
        //   relativeTo: this.acRoute,
        //   queryParams: queryParams,
        //   queryParamsHandling: 'merge',
        // })
        this.router.navigate(['/page/tabs/cholesterol'])
      }

    }
    else if (role == 'ROLE_DIETITIAN') {
      this.router.navigate(['/page/worklist'])
    }
  }

  logout() {
    this.router.navigate([`login`]);
  }

  //test kit id search
  search(form: NgForm): void {
    let url = 'api/admin/testkit/search';
    var kitId = form.value.testkit.replace(/\D/g, "")
    if (form.valid) {
      let body = {
        testkit_id: kitId
      }
      this.http.post(url, body).subscribe((response: any) => {
        this.testkit_data = response;
        this.completed_status = this.testkit_data.completed
        // console.log('this.testkit_data',this.testkit_data.completed)
        localStorage.setItem('sidebar_id', this.testkit_data.id)
        localStorage.setItem('examination_id', this.testkit_data.id)
        JSON.stringify(localStorage.setItem('testkit_id', JSON.stringify(this.testkit_data)))
        localStorage.setItem('UserId', this.testkit_data.id)
        if (response.status == 1) {
          this.testKit_error = false
        }
        if (response.status == 0) {
          this.testKit_error = true
          this.errorFunction(response)
        }
      })
    }
  }

  errorFunction(data: any) {
    console.log(data)
    var message = data.message.name
    if (data.message.name === 'TokenExpiredError') {
      localStorage.clear();
      this.router.navigate([`login`]);
    }
  }


}
