import { Injectable } from '@angular/core';
import {
  NbComponentStatus,
  NbGlobalPhysicalPosition,
  NbToastrService,
} from "@nebular/theme";

@Injectable({
  providedIn: 'root'
})
export class UiserviceService {

  constructor(private toastrService: NbToastrService) { }

  showToast(
    type: NbComponentStatus,
    title: string,
    body: string,
    time: number
  ) {
    const config = {
      status: type,
      duration: time,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    this.toastrService.show(body, title, config);
  }
}
