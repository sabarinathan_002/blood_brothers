import { Injectable } from '@angular/core';
import { HttpService } from './http.service'

@Injectable({
  providedIn: 'root'
})
export class DataService {
  itemsArray: Array<any> = [];
  items: Array<any> = []
  counter: number = 1;

  constructor(private http: HttpService) { }


  sidebardetails() {

    this.http.get('api/admin/examination/list/1').subscribe((response: any) => {
      console.log('length', response.data.length)
      for (let i = 0; i < response.data.length; i++) {
        console.log()
        var data = response.data[i]
        var link = "/page/tabs/cholesterol?id=" + data.id
        // var link = '/page/tabs/cholesterol'
        var body;
        if (i == 0) {
          body = {
            title: data.test_name,
            url: link,
            home: true,
          }
        }
        else {
          body = {
            title: data.test_name,
            url: link
          }
        }
        this.itemsArray.push(body)
      }
      this.items = this.itemsArray
    })

  }

  

}
