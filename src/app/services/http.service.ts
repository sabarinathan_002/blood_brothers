import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private accessToken: any ;

  constructor(private http: HttpClient) { 
    
  }

  setHeader(header:HttpHeaders){
    var token = localStorage.getItem('access_token')!
    header.set('Content-Type', 'application/json')
      .set('X-access-token', token);
      return header;
  }
  

  ipaddress() {
    const ip1 = 'http://18.118.128.52:8080/';
    const ip2  = "http://54.170.169.40:8080/"
    // const ip3  = "https://54.170.169.40:8080/"

    return ip1
  }

  get(url: string) {
    var token = localStorage.getItem('access_token')!
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
      .set('X-access-token', token);
    return this.http.get(this.ipaddress() + url, {
      headers: headers,
    });
  }

  post(url: string, body: any) {
    var token = localStorage.getItem('access_token')!
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
      .set('X-access-token', token);
    return this.http.post(this.ipaddress() + url, body, {
      headers: headers,
    });
  }

  login(url: string, body: any) {
    return this.http.post(this.ipaddress() + url, body, {
    });
  }

  getDietitianDatas(url: string){
    var token = localStorage.getItem('access_token')!
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
      .set('X-access-token', token);
    return this.http.get(this.ipaddress() + url, {
      headers: headers,
    })
  }
  
  
}
