import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SearchComponent } from './search/search.component';
import {AuthguardGuard} from './services/authguard.guard'

const routes: Routes = [
  {
    path: 'page',
    canActivate: [AuthguardGuard],
    loadChildren: () => import('./page/page.module')
      .then(m => m.PageModule),
  },
  { path: 'login', component: LoginComponent },
  { path: 'search', canActivate: [AuthguardGuard], component: SearchComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '**', redirectTo: 'page' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
